# wsdm-tooltip

Tooltip component for d3 charts.

# Usage

Create tooltip (with optional configuration):

```
const tip = tooltip({
  className: "custom-tooltip",
  styles: {
    color: "#eee",
    "font-size": "0.75rem",
    "background-color": "rgba(0,0,0,.9)",
    "border-radius": "3px",
  },
})
```

Show tooltip with the content (text or html string):

```
tip.show(content)
```

Position tooltip (with optional coordinates object and offset array):
Coordinates object defaults to d3's `event`, default offset is `[0, -5]`

```
tip.position({ pageX, pageY }, [x, y])
```

Hide tooltip:

```
tip.hide()
```

Run development server with:

```
$ npm i
$ make start
```

Build with:

```
$ make build
```

[Lapidus Interactive](http://lapidus.se)
