import { event } from "d3-selection"

const tooltip = (options = {}) => {
  let tip = null
  let size = null
  let resizeLimit = 10
  let resizeCount = 0
  const defaults = {
    className: "wsdm-tooltip",
    styles: {
      display: "none",
      "z-index": 9999,
      padding: "0.5rem 0.5rem 0.35rem",
      "background-color": "rgba(255,255,255, 0.9)",
    },
  }

  create(options)

  /**
   * Create and/or setup the tooltip element
   * @param  {Object} options tooltip's options
   * @return {Void}
   */
  function create(options = {}) {
    const className = options.className || defaults.className
    tip = document.querySelector(`.${className}`)

    if (!tip) {
      tip = document.createElement("div")
      tip.className = className
      document.body.appendChild(tip)
    }

    const styles = Object.assign(defaults.styles, options.styles)
    for (const key in styles) {
      if (styles.hasOwnProperty(key)) {
        tip.style[key] = styles[key]
      }
    }
  }

  /**
   * Show the tooltip with the specified content
   * @param  {String} content tooltip's content
   * @return {Void}
   */
  function show(content) {
    tip.style.display = "block"
    tip.style.position = "absolute"
    tip.innerHTML = `${content}`

    // Measure the size
    size = tip.getBoundingClientRect()
  }

  /**
   * Hide the tooltip
   * @return {Void}
   */
  function hide() {
    tip.innerHTML = ""
    tip.style.display = "none"
    size = null
    resizeCount = 0
  }

  /**
   * Position the tooltip on the page
   * based on the d3 event mouse position
   * @param  {Object} event d3 current event, optional
   * @param  {Array} offset tooltip's offset [x, y], optional
   * @return {Void}
   */
  function position(e = event, offset = [0, -5]) {
    if (!tip || !size) return

    const { pageX, pageY } = e
    const { width, height } = size

    tip.style.left = `${pageX - width / 2 + offset[0]}px`
    tip.style.top = `${pageY - height + offset[1]}px`

    // Make sure positioning didn't change the size
    // if so, set it again
    // resizeLimit prevents infinite loops
    const newSize = tip.getBoundingClientRect()
    if (
      resizeCount < resizeLimit &&
      (newSize.width !== width || newSize.height !== height)
    ) {
      resizeCount += 1
      size = newSize
      position(e, offset)
    }
  }

  return { create, show, hide, position }
}

export default tooltip
