import { scaleLinear, select, event, max } from "d3"
import tooltip from ".."

const tip1 = tooltip()
const tip2 = tooltip({
  className: "custom-tooltip",
  styles: {
    color: "#eee",
    "font-size": "0.75rem",
    "background-color": "rgba(0,0,0,.9)",
    "border-radius": "3px",
  },
})

const data = [
  { name: "Locke", value: 4 },
  { name: "Reyes", value: 8 },
  { name: "Ford", value: 15 },
  { name: "Jarrah", value: 16 },
  { name: "Shephard", value: 23 },
  { name: "Kwon", value: 42 },
]

makeChart(".chart1", tip1, data)
makeChart(".chart2", tip2, data)

function makeChart(selector, tip, data) {
  const width = 420
  const barHeight = 20

  const x = scaleLinear()
    .range([0, width])
    .domain([0, max(data, d => d.value)])

  const chart = select(selector)
    .attr("width", width)
    .attr("height", barHeight * data.length)

  const bar = chart
    .selectAll("g")
    .data(data)
    .enter()
    .append("g")
    .attr("transform", (d, i) => `translate(0, ${i * barHeight})`)
    .on("mouseenter", d => tip.show(`${d.name}, ${d.value}`))
    .on("mousemove", d => tip.position(event))
    .on("mouseleave", d => tip.hide())

  bar
    .append("rect")
    .attr("width", d => x(d.value))
    .attr("height", barHeight - 1)

  bar
    .append("text")
    .attr("x", d => x(d.value) - 3)
    .attr("y", barHeight / 2)
    .attr("dy", ".35em")
    .text(d => d.value)
}
